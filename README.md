# ffmpeg_4.1_hevc_rtmp_functioned


#### 介绍
总结网上各路大神对于ffmpeg推hevc的rtmp流的教程介绍，发现ffmpeg编译还有很多坑的，我在这里准备了一套文件把坑给填上。  
实现效果：ffmpeg可以推送HEVC的rtmp流  
运行环境centos7  

#### 软件架构
调试过的安装脚本+文件压缩包 形成自解压自运行的开箱脚本。


#### 安装教程

1.  chmod +x ffmpeg.run
2.  ./ffmpeg.run

#### 使用说明
后台推流命令  
nohup ffmpeg -nostdin -i "in" -c:v copy -c:a libfdk_aac -f flv "rtmp://out"  
其中输入in可用网络流或者文件，如使用文件，在ffmpeg后加入参数`-re --fflags +genpts`  
输出rtmp://out请自行使用可用地址，如果没有，可以移步我搭建的有推拉流验证的srs项目[srs-auth-with-python](https://gitee.com/gunaiqiu/srs-auth-with-python)，搭建后用以下命令测试：  
  
推流地址：rtmp://127.0.0.1/live?do=publish&key=2cccb9bbb7b0f20a916233e38899c0f9/test  
拉流地址：rtmp://127.0.0.1/live?do=play&key=287d2df531c43e8abb4aab223a9aaa61/test  
